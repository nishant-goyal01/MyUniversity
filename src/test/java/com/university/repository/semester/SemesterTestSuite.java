package com.university.repository.semester;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({SemesterDaoIntegrationTest.class,AggregateDaoIntegrationTest.class})
public class SemesterTestSuite {

}
