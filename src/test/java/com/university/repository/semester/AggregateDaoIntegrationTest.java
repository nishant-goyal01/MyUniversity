package com.university.repository.semester;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.university.models.Semester;
import com.university.repository.SemesterDao;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class AggregateDaoIntegrationTest {

	@Autowired
	SemesterDao semesterDao;
	
	
	@Test
	public void findByDepartment() {
		List<Semester> lstPer =  semesterDao.findBySno(1);
		//double avg = lstPer.stream().mapToDouble(x -> x.getPercentage()).average().getAsDouble();
		//assertThat(lstPer).hasSize(1);
	}
}
