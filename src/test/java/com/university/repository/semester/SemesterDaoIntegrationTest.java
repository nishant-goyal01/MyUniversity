package com.university.repository.semester;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.university.models.Semester;
import com.university.repository.SemesterDao;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class SemesterDaoIntegrationTest {

	@Autowired
	SemesterDao semesterDao;
	
	@Test
	public void save() {
		Long sno ;
		Semester semester = new Semester(1, "1 sem", 60.00);
		semesterDao.save(semester);
		sno = semester.getSno();
		assertNotNull(sno);
	}

}
