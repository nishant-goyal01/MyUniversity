package com.university.repository.student;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({StudentDaoIntegrationTest.class,ByDeptDaoIntegrationTest.class})
public class StudentTestSuite {

}
