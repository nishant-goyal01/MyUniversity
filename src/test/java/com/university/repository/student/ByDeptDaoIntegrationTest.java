package com.university.repository.student;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.university.models.Student;
import com.university.repository.StudentDao;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class ByDeptDaoIntegrationTest {

	@Autowired
	StudentDao studentDao;


	@Test
	public void findByDepartment() {
		List<Student> lstStudents = studentDao.findByDepartment("dept");
		//assertThat(lstStudentName).hasSize(1);
	}


}
