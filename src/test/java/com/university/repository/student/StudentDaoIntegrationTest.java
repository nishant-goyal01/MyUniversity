package com.university.repository.student;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.university.models.Student;
import com.university.repository.StudentDao;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
//@WebAppConfiguration
public class StudentDaoIntegrationTest {

	@Autowired
	StudentDao studentDao;
	
	@Test
	public void save() {
		Long sno ;
		Student student = new Student("name",  "dept");
		studentDao.save(student);
		sno = student.getSno();
		assertNotNull(sno);
	}

	

	
	
	/*@Autowired
	private WebApplicationContext context;

	private MockMvc mvc;
	
	@Before
	public void setUp() {
		this.mvc = MockMvcBuilders.webAppContextSetup(this.context).build();
	}
	
	@Test
	public void contextLoads() throws Exception {
		this.mvc.perform(get("/")).andExpect(status().isOk())
		.andExpect(xpath("//tbody/tr").nodeCount(4));
	}*/

	
	
	
	
	

}
