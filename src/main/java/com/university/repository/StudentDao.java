package com.university.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.university.models.Student;


public interface StudentDao extends CrudRepository<Student, Long> {



	public List<Student> findByDepartment(String department);

	public Student save(Student student);
}
