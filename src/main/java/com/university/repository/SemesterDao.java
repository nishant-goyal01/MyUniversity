package com.university.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.university.models.Semester;

public interface SemesterDao extends CrudRepository<Semester, Long> {

	public List<Semester> findBySno(long sno);

	public  Semester save(Semester semester);
}
