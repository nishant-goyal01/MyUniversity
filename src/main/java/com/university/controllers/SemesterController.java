package com.university.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.university.models.Semester;
import com.university.repository.SemesterDao;

@Controller
@RequestMapping("/semester")
public class SemesterController {

	@Autowired
	SemesterDao semesterDao;

	// for URL  http://localhost:8080/semester/save?sno=1&semester="1-Sem"&percentage=80
	// save semester result for a student it in the database.

	@RequestMapping("/save")
	@Transactional
	@ResponseBody
	public String save(long sno, String semester, Double percentage) {
		Long uid ;
		try {
			Semester semesterObj = new Semester(sno,  semester,  percentage);
			semesterDao.save(semesterObj);
			uid = semesterObj.getUid();
		}
		catch (Exception ex) {
			ex.printStackTrace();
			return "Error in saving the semester result for a student: " + ex.toString();
		}
		return "Succesfully saved semester result for the student";
	}

	// for URL  http://localhost:8080/semester/aggregate?sno=1
	// get aggregate result for all semesters for a student it in the database.

	@RequestMapping("/aggregate")
	@Transactional
	@ResponseBody
	public String aggregate(long sno) {
		double avg ;
		try {

			List<Semester> lstPer =  semesterDao.findBySno(sno);
			avg = lstPer.stream().mapToDouble(x -> x.getPercentage()).average().getAsDouble();

		}
		catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
			//return "Error in get aggregate result for all semesters for a student: " + ex.toString();
		}
		return "Aggregated result of all semesters for student with id as "+sno+" is = " + avg;
	}


}
