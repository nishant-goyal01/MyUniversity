package com.university.controllers;

import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.university.models.Student;
import com.university.repository.StudentDao;

@Controller
@RequestMapping("/student")
public class StudentController {

	@Autowired
	StudentDao studentDao;


	// URl: http://localhost:8080/student/save?name="John"&department="ML"	
	// Create a new user and save it in the database.

	@RequestMapping("/save")
	@Transactional
	//@RestController
	@ResponseBody
	public String save(String name, String department) {
		Long sno ;
		try {
			Student student = new Student(name,  department);
			studentDao.save(student);
			sno = student.getSno();
		}
		catch (Exception ex) {
			ex.printStackTrace();
			return "Error saving the student details: " + ex.toString();
		}
		return "student details succesfully saved with id = " + sno;
	}

	// URL: http://localhost:8080/student/get-by-department?deptName="ML"
	// Returns the name for the students in a department.

	@RequestMapping("/get-by-department")
	@Transactional
	@ResponseBody
	public String getByDepartment(String deptName) {
		StringBuilder  students =  new StringBuilder();
		try {
			List<Student> lstStudents = studentDao.findByDepartment(deptName);
			List<String> lstNames = lstStudents.stream().map(s -> s.getName()).collect(Collectors.toList());
			for(String stName: lstNames){
				students.append(stName +"  ");
			}
		}
		catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
			//return "Error  in fetching the student list: " + ex.toString();
		}
		return "The list of Student for "+deptName+" depatment is : " + students;
	}

}
