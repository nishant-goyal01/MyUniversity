package com.university.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "semester")
public class Semester {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long uid;


	@NotNull
	private long sno;

	@NotNull
	private String semester ;

	@NotNull
	private Double percentage;

	public Semester(){

	}

	public Semester(long sno, String semester, Double percentage) {
		super();
		this.sno = sno;
		this.semester = semester;
		this.percentage = percentage;
	}

	public long getSno() {
		return sno;
	}

	public void setSno(long sno) {
		this.sno = sno;
	}

	public String getSemester() {
		return semester;
	}

	public void setSemester(String semester) {
		this.semester = semester;
	}

	public Double getPercentage() {
		return percentage;
	}

	public void setPercentage(Double percentage) {
		this.percentage = percentage;
	}

	public long getUid() {
		return uid;
	}

	public void setUid(long uid) {
		this.uid = uid;
	}


}
