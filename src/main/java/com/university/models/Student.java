package com.university.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;





@Entity
@Table(name = "student")
public class Student {


	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long sno;

	@NotNull
	private String name;

	@NotNull
	private String department;
	
	public long getSno() {
		return sno;
	}

	public void setSno(long sno) {
		this.sno = sno;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}



	public Student() { }

	public Student(String name, String department) { 
		//this.sno = id;
		this.name = name;
		this.department = department;
	}
}
